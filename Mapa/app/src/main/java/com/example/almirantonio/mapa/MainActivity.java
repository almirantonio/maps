package com.example.almirantonio.mapa;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class MainActivity extends FragmentActivity implements GoogleMap.OnMapClickListener {
    SupportMapFragment mapFragment;
    LatLng fafica=new LatLng(-8.298635, -35.974063);
    LatLng RuaPedroPadeiro=new LatLng(-7.9876198,-36.4960746);
    LatLng ruaSebastiao=new LatLng(-7.9884778,-36.4976244);
   static CameraUpdate update;
    GoogleMap mapaTela;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.maps);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
               iniciarMapa(googleMap);
               Mapa(googleMap);

            }
        });

    }
public void Mapa(GoogleMap mapa){
        mapaTela=mapa;
}

    @Override
    public void onMapClick(LatLng latLng) {

    }
    private void iniciarMapa(GoogleMap map){
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);


        update= CameraUpdateFactory.newLatLngZoom(fafica,18);

        map.animateCamera(update);

    }

    public void jatauba(View view) {
        mapaTela.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        LatLng jatauba=new LatLng(-7.9876198, -36.4960746);

        update= CameraUpdateFactory.newLatLngZoom(jatauba,18);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(jatauba).title("Casa").snippet("Minha Residência");

        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.casa));
        mapaTela.addMarker(markerOptions);
        mapaTela.animateCamera(update);

    }


    public void zoomMais(View view) {

            update = CameraUpdateFactory.zoomBy( 1);
            mapaTela.animateCamera(update);


    }

    public void zoomMenos(View view) {

        update = CameraUpdateFactory.zoomBy(-1);
        mapaTela.animateCamera(update);
    }

    public void tipo(View view) {
        mapaTela.setMapType(GoogleMap.MAP_TYPE_HYBRID);
    }

    public void linha(View view) {
        update= CameraUpdateFactory.newLatLngZoom(RuaPedroPadeiro,18);
        mapaTela.animateCamera(update);

        PolylineOptions line = new PolylineOptions();
        line.add(new LatLng(-7.9876198,-36.4960746));
        line.add(new LatLng(-7.9884778,-36.4976244));
        line.color(Color.BLUE);
        Polyline polyline = mapaTela.addPolyline(line);

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(ruaSebastiao).title("Rua São Sebastião").snippet("           Centro");


        mapaTela.addMarker(markerOptions);

        MarkerOptions markerOptionsCasa = new MarkerOptions();
        markerOptionsCasa.position(RuaPedroPadeiro).title("Casa").snippet("Minha Residência");
        markerOptionsCasa.icon(BitmapDescriptorFactory.fromResource(R.drawable.casa));

        mapaTela.addMarker(markerOptionsCasa);
    }

    public void googleMaps(View view) {
        Uri uriGeografico = Uri.parse("geo:0,0?q=rua pedro padeiro, Jataúba");
        Intent it = new Intent(Intent.ACTION_VIEW, uriGeografico);
        startActivity(it);
    }
}
